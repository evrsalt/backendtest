-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: backend
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `channels`
--

DROP TABLE IF EXISTS `channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `channels` (
  `idChannel` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `picture` varchar(45) NOT NULL,
  `parentChannel` int(11) DEFAULT NULL,
  PRIMARY KEY (`idChannel`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channels`
--

LOCK TABLES `channels` WRITE;
/*!40000 ALTER TABLE `channels` DISABLE KEYS */;
INSERT INTO `channels` VALUES (1,'Musica','musica',1),(2,'Prensa & Revistas','revistas',2),(3,'Peliculas','peliculas',3),(4,'Niños','ninos',4),(5,'Series','series',5),(6,'Guias','guias',6),(7,'Un Viaje A Nantes','picture',5),(8,'Sportactive','picture',5),(9,'Atresplayer','atresplayer',5),(10,'RedBull TV','picture',5),(11,'ElClubDeLaComedia','clubcomedia',9),(12,'El Objetivo','picture',9);
/*!40000 ALTER TABLE `channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channels_contents`
--

DROP TABLE IF EXISTS `channels_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `channels_contents` (
  `idChannel` int(11) NOT NULL,
  `idContent` int(11) NOT NULL,
  PRIMARY KEY (`idChannel`,`idContent`),
  KEY `FK_IDCONTENT_idx` (`idContent`),
  CONSTRAINT `FK_IDCHANNELCONTENTS` FOREIGN KEY (`idChannel`) REFERENCES `channels` (`idChannel`),
  CONSTRAINT `FK_IDCONTENTCHANNELS` FOREIGN KEY (`idContent`) REFERENCES `contents` (`idContent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channels_contents`
--

LOCK TABLES `channels_contents` WRITE;
/*!40000 ALTER TABLE `channels_contents` DISABLE KEYS */;
INSERT INTO `channels_contents` VALUES (11,1),(11,2),(12,3),(12,4);
/*!40000 ALTER TABLE `channels_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channels_languages`
--

DROP TABLE IF EXISTS `channels_languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `channels_languages` (
  `idChannel` int(11) NOT NULL,
  `idLanguage` int(11) NOT NULL,
  PRIMARY KEY (`idChannel`,`idLanguage`),
  KEY `FK_IDLANGUAGE_idx` (`idLanguage`),
  CONSTRAINT `FK_IDCHANNEL` FOREIGN KEY (`idChannel`) REFERENCES `channels` (`idChannel`),
  CONSTRAINT `FK_IDLANGUAGE` FOREIGN KEY (`idLanguage`) REFERENCES `languages` (`idLanguage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channels_languages`
--

LOCK TABLES `channels_languages` WRITE;
/*!40000 ALTER TABLE `channels_languages` DISABLE KEYS */;
INSERT INTO `channels_languages` VALUES (7,1),(8,1),(9,1),(7,2),(10,2);
/*!40000 ALTER TABLE `channels_languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents`
--

DROP TABLE IF EXISTS `contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contents` (
  `idContent` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idContent`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents`
--

LOCK TABLES `contents` WRITE;
/*!40000 ALTER TABLE `contents` DISABLE KEYS */;
INSERT INTO `contents` VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10);
/*!40000 ALTER TABLE `contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `files` (
  `idFile` int(11) NOT NULL AUTO_INCREMENT,
  `filePath` varchar(45) NOT NULL,
  `idContent` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  PRIMARY KEY (`idFile`),
  KEY `FK_IDCONTENT_FILE_idx` (`idContent`),
  CONSTRAINT `FK_IDCONTENT_FILE` FOREIGN KEY (`idContent`) REFERENCES `contents` (`idContent`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` VALUES (1,'dGVqUF-IEyI',1,10),(2,'7q3e4x6w4AE',2,5),(3,'textPath',1,5),(4,'pdfpath',2,7),(5,'path2',3,3),(6,'path3',3,5),(7,'path4',4,8),(8,'path5',4,2);
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `languages` (
  `idLanguage` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(45) NOT NULL,
  PRIMARY KEY (`idLanguage`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (1,'Spanish'),(2,'English');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metadata`
--

DROP TABLE IF EXISTS `metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `metadata` (
  `idMetadata` int(11) NOT NULL AUTO_INCREMENT,
  `metadata` varchar(150) NOT NULL,
  `idContent` int(11) NOT NULL,
  PRIMARY KEY (`idMetadata`),
  KEY `FK_IDCONTENT_idx` (`idContent`),
  CONSTRAINT `FK_IDCONTENT` FOREIGN KEY (`idContent`) REFERENCES `contents` (`idContent`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metadata`
--

LOCK TABLES `metadata` WRITE;
/*!40000 ALTER TABLE `metadata` DISABLE KEYS */;
INSERT INTO `metadata` VALUES (1,'Lorem ipsum dolor sit amet consectetur adipiscing elit lectus suscipit sapien ad mus, phasellus per scelerisque penatibus vivamus posuere purus velit',1),(2,'dolor sit amet consectetur adipiscing elit lectus suscipit sapien ad mus, phasellus per scelerisque penatibus ',2);
/*!40000 ALTER TABLE `metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating_channels`
--

DROP TABLE IF EXISTS `rating_channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rating_channels` (
  `idRatingChannels` int(11) NOT NULL AUTO_INCREMENT,
  `ratingValue` int(11) NOT NULL,
  `idChannel` int(11) NOT NULL,
  PRIMARY KEY (`idRatingChannels`),
  KEY `FK_IDRATINGCHANNEL_idx` (`idChannel`),
  CONSTRAINT `FK_IDRATINGCHANNEL` FOREIGN KEY (`idChannel`) REFERENCES `channels` (`idChannel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating_channels`
--

LOCK TABLES `rating_channels` WRITE;
/*!40000 ALTER TABLE `rating_channels` DISABLE KEYS */;
/*!40000 ALTER TABLE `rating_channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ratings_content`
--

DROP TABLE IF EXISTS `ratings_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ratings_content` (
  `idRatingContent` int(11) NOT NULL AUTO_INCREMENT,
  `ratingValue` int(11) NOT NULL,
  `idContent` int(11) NOT NULL,
  PRIMARY KEY (`idRatingContent`),
  KEY `FK_IDCONTENT_RATING_idx` (`idContent`),
  CONSTRAINT `FK_IDCONTENT_RATING` FOREIGN KEY (`idContent`) REFERENCES `contents` (`idContent`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ratings_content`
--

LOCK TABLES `ratings_content` WRITE;
/*!40000 ALTER TABLE `ratings_content` DISABLE KEYS */;
INSERT INTO `ratings_content` VALUES (1,8,1),(2,6,2),(3,2,3),(4,5,4);
/*!40000 ALTER TABLE `ratings_content` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-08 23:54:52
