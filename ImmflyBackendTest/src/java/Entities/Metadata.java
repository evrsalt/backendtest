package Entities;

public class Metadata {

    private int idMetadata;
    private String metadata;
    private int idFile;

    public int getIdMetadata() {
        return idMetadata;
    }

    public void setIdMetadata(int idMetadata) {
        this.idMetadata = idMetadata;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public int getIdFile() {
        return idFile;
    }

    public void setIdFile(int idFile) {
        this.idFile = idFile;
    }

    public Metadata() {
    }

    public Metadata(int idMetadata, String metadata, int idFile) {
        this.idMetadata = idMetadata;
        this.metadata = metadata;
        this.idFile = idFile;
    }

}
